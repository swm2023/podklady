# Gitlab CI hands on - part II (6th lecture 2023-04-13)

1. Extend the CI with the following steps:
   1. Introduce a step which will modify footer with the built date.
      1. Create folder `scripts`.
      1. Create a BASH script to replace string.
      1. Do not forget to add execute bit to the script.
      1. Add it to pages step.
   1. Remove that version script line and make it a separate CI job.
   1. Extend the CI with Markdown lint.
   1. Extend the CI with post deploy script to check external links. (optional)


Project example:

```bash
$ tree  -L 1
.
├── docs.it4i
├── material
├── mkdocs.yml
├── package.json
├── pathcheck.sh
├── README.md
├── requirements.txt
├── scripts
├── site
├── snippets
└── venv
```

Bash example - add_version.sh

```bash
#!/bin/bash
VER=$(git log --pretty=format:'/ ver. %h / %ai' -n 1)
sed "s,__VERSION__, $VER," mkdocs.yml -i
YEAR=$(date +"%Y")
sed "s,__YEAR__, $YEAR," mkdocs.yml -i
```

MKDocs config:

```yaml
copyright: Copyright (c) 2013-__YEAR__ IT4Innovations__VERSION__
```

Gitlab CI config examples:

```yaml
  - bash scripts/add_version.sh
```

```yaml
capitalize:
  stage: test
  image: it4innovations/docker-mkdocscheck:latest
  script:
  - find mkdocs.yml docs.it4i/ \( -name '*.md' -o -name '*.yml' \) -print0 | xargs -0 -n1 scripts/titlemd.py --test
```

```yaml
docs:
  stage: test
  image: it4innovations/docker-mdcheck:latest
  allow_failure: true
  script:
  - mdl -r ~MD013,~MD010,~MD014,~MD024,~MD026,~MD029,~MD033,~MD036,~MD037,~MD046 *.md docs.it4i # BUGS
```

```yaml
ext_links:
  stage: after_test
  image: it4innovations/docker-mdcheck:latest
  allow_failure: true
  after_script:
  # remove JSON results
  - rm *.json
  script:
  - find docs.it4i/ -name '*.md' -exec grep --color -l http {} + | xargs awesome_bot -t 10 --allow-dupe --allow-redirect
  only:
  - master
```


# Gitlab CI hands on (7th lecture 2023-04-17)

Create a GIT repository with Python code and unit tests. Files are in 09_handson folder.

1. Create a repo
1. Create a virtual environment for python `virtualenv venv` or `virtualenv venv -p python3`.
1. Activate virtual environment `source venv/bin/activate`.
1. Introduce proper `.gitignore` file.
1. Fill it with Pythons files. (Move to scr folder).
   ```console
   https://gitlab.com/-/snippets/2284564
   https://gitlab.com/-/snippets/2284559
   ```

   You can use `wget` to download the files easilly. Mind the proper URL, you want the raw URLs...

   ```console
   wget https://gitlab.com/-/snippets/2284559/raw/main/user.py
   wget https://gitlab.com/-/snippets/2284564/raw/main/test_user.py
   ```

1. Try running `pytest` in current folder.
1. Try to find how to run coverage with `pytest-cov`
1. Try to find how to create HTML report for Pytest and coverage. (https://pytest-cov.readthedocs.io/en/latest/ and https://pytest-html.readthedocs.io/en/latest/)
1. Create a CI with 2 tasks:
   1. Task to create HTML report of unittest and code coverage.
      1. You need to install `pytest-cov` and `pytest-html`.
      1. You need to find out how to create reports to `public` folder. You want to make a Gitlab pages out of it!
   1. Task to get XML report of code coverage and introduce proper `coverage` regex.
      1. Make use of code coverage report:
      ```console
      coverage run -m pytest
      coverage report
      coverage xml
      ```
      1. Create regex for use in `coverage` directive in `.gitlab-ci.yml`. (https://docs.gitlab.com/ee/ci/yaml/index.html#coverage and https://regex101.com/r/gpVjKW/1)
      1. Create XML coverage report file for `cobertura` (https://docs.gitlab.com/ee/user/project/merge_requests/test_coverage_visualization.html#python-example)
1. Once having CI up and running create an issue template with the following content:
   (Create folder `.gitlab/issue_templates/` see https://docs.gitlab.com/ee/user/project/description_templates.html#create-an-issue-template)
   ```markdown
   * [ ] Verify Gitlab pages link to unittest HTML report.
   * [ ] Verify Gitlab pages link to code coverage HTML report.
   * [ ] Create a new brach and modify code with a new method, e.g.:
         ```python
         def deactivate(self):
            self.profile['active'] = False
         ```
   * [ ] Verify the new MR is having code coverage percentage report.
   * [ ] Create README.md and add project badges to it:
     * [ ] CI status
     * [ ] Code coverage status
   ```
1. Create ticket based on the template.
1. Go over the tasks in the ticket and proceed the steps.
